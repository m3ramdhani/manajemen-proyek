/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100116
Source Host           : localhost:3306
Source Database       : db_rocky

Target Server Type    : MYSQL
Target Server Version : 100116
File Encoding         : 65001

Date: 2017-08-22 06:54:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for detail_laporan
-- ----------------------------
DROP TABLE IF EXISTS `detail_laporan`;
CREATE TABLE `detail_laporan` (
  `id_resiko` int(11) DEFAULT NULL,
  `id_laporan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_laporan
-- ----------------------------
INSERT INTO `detail_laporan` VALUES ('5', '3');
INSERT INTO `detail_laporan` VALUES ('6', '3');

-- ----------------------------
-- Table structure for enum_hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `enum_hak_akses`;
CREATE TABLE `enum_hak_akses` (
  `id_hak_akses` int(2) NOT NULL,
  `nama_hak_akses` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_hak_akses`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of enum_hak_akses
-- ----------------------------
INSERT INTO `enum_hak_akses` VALUES ('1', 'Admin');
INSERT INTO `enum_hak_akses` VALUES ('2', 'Manajer');

-- ----------------------------
-- Table structure for enum_status_proyek
-- ----------------------------
DROP TABLE IF EXISTS `enum_status_proyek`;
CREATE TABLE `enum_status_proyek` (
  `id_status` int(5) NOT NULL,
  `nama_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of enum_status_proyek
-- ----------------------------
INSERT INTO `enum_status_proyek` VALUES ('1', 'Baru');
INSERT INTO `enum_status_proyek` VALUES ('2', 'Prioritas');
INSERT INTO `enum_status_proyek` VALUES ('3', 'Running');
INSERT INTO `enum_status_proyek` VALUES ('4', 'Selesai');

-- ----------------------------
-- Table structure for hasil_perhitungan_prioritas
-- ----------------------------
DROP TABLE IF EXISTS `hasil_perhitungan_prioritas`;
CREATE TABLE `hasil_perhitungan_prioritas` (
  `id_hasil_perhitungan_prioritas` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL,
  PRIMARY KEY (`id_hasil_perhitungan_prioritas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hasil_perhitungan_prioritas
-- ----------------------------

-- ----------------------------
-- Table structure for jadwal
-- ----------------------------
DROP TABLE IF EXISTS `jadwal`;
CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyek` int(11) NOT NULL,
  `tasklist` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `predecessor` varchar(25) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jadwal
-- ----------------------------
INSERT INTO `jadwal` VALUES ('1', '4', 'Persiapan', '2017-07-01', '2017-07-01', null, '1', 'A');
INSERT INTO `jadwal` VALUES ('7', '4', 'Analisis Sistem dan desain', '2017-07-02', '2017-07-20', '1', '14', 'B');
INSERT INTO `jadwal` VALUES ('8', '4', 'Frontend Development', '2017-07-21', '2017-08-31', '1', '30', 'C');
INSERT INTO `jadwal` VALUES ('9', '4', 'Backend development', '2017-09-01', '2017-11-23', '7,8', '60', 'D');
INSERT INTO `jadwal` VALUES ('10', '4', 'Quality Assurance', '2017-11-24', '2017-12-04', '9', '7', 'E');
INSERT INTO `jadwal` VALUES ('11', '4', 'Technical Support', '2017-12-05', '2017-12-11', '10', '5', 'F');

-- ----------------------------
-- Table structure for laporan_proyek
-- ----------------------------
DROP TABLE IF EXISTS `laporan_proyek`;
CREATE TABLE `laporan_proyek` (
  `id_laporan` int(15) NOT NULL AUTO_INCREMENT,
  `id_proyek` int(15) DEFAULT NULL,
  `deskripsi_laporan` varchar(255) DEFAULT NULL,
  `tanggal_laporan` date DEFAULT NULL,
  PRIMARY KEY (`id_laporan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of laporan_proyek
-- ----------------------------
INSERT INTO `laporan_proyek` VALUES ('1', '5', 'aman', '2017-07-20');
INSERT INTO `laporan_proyek` VALUES ('2', '5', 'aman lagi nih', '2017-07-21');
INSERT INTO `laporan_proyek` VALUES ('3', '5', 'waduh kacau nih', '2017-07-23');

-- ----------------------------
-- Table structure for manpower
-- ----------------------------
DROP TABLE IF EXISTS `manpower`;
CREATE TABLE `manpower` (
  `id_manpower` int(2) NOT NULL AUTO_INCREMENT,
  `kriteria_manpower` varchar(50) NOT NULL,
  `bobot_manpower` int(2) NOT NULL,
  `value` int(2) NOT NULL,
  PRIMARY KEY (`id_manpower`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of manpower
-- ----------------------------
INSERT INTO `manpower` VALUES ('1', '5 Orang', '1', '5');
INSERT INTO `manpower` VALUES ('2', '4 Orang', '2', '4');
INSERT INTO `manpower` VALUES ('3', '3 Orang', '3', '3');
INSERT INTO `manpower` VALUES ('4', '2 Orang', '4', '2');
INSERT INTO `manpower` VALUES ('5', '1 Orang', '5', '1');

-- ----------------------------
-- Table structure for platform
-- ----------------------------
DROP TABLE IF EXISTS `platform`;
CREATE TABLE `platform` (
  `id_platform` int(2) NOT NULL AUTO_INCREMENT,
  `kriteria_platform` varchar(50) NOT NULL,
  `bobot_platform` int(2) NOT NULL,
  PRIMARY KEY (`id_platform`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of platform
-- ----------------------------
INSERT INTO `platform` VALUES ('1', 'Web ', '3');
INSERT INTO `platform` VALUES ('2', 'Mobile', '2');
INSERT INTO `platform` VALUES ('3', 'Desktop', '1');

-- ----------------------------
-- Table structure for price
-- ----------------------------
DROP TABLE IF EXISTS `price`;
CREATE TABLE `price` (
  `id_price` int(2) NOT NULL AUTO_INCREMENT,
  `kriteria_price` varchar(50) NOT NULL,
  `bobot_price` int(2) NOT NULL,
  `min_price` double DEFAULT NULL,
  `max_price` double DEFAULT NULL,
  PRIMARY KEY (`id_price`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of price
-- ----------------------------
INSERT INTO `price` VALUES ('1', '<=50juta', '1', '1', '50000000');
INSERT INTO `price` VALUES ('2', '>50jt - 100jt', '2', '51', '100000000');
INSERT INTO `price` VALUES ('3', '>100jt - 250jt', '3', '100000001', '250000000');
INSERT INTO `price` VALUES ('4', '>250jt - 400jt', '4', '250000001', '400000000');
INSERT INTO `price` VALUES ('5', '>400jt - 700jt', '5', '400000001', '700000000');
INSERT INTO `price` VALUES ('6', '>700jt', '6', '700000001', '999999999999');

-- ----------------------------
-- Table structure for proyek
-- ----------------------------
DROP TABLE IF EXISTS `proyek`;
CREATE TABLE `proyek` (
  `id_proyek` int(5) NOT NULL AUTO_INCREMENT,
  `nama_proyek` varchar(50) NOT NULL,
  `pic_proyek` varchar(50) NOT NULL,
  `alamat_proyek` text NOT NULL,
  `telp_proyek` int(15) NOT NULL,
  `harga_proyek` varchar(50) NOT NULL,
  `mulai_proyek` date NOT NULL,
  `berakhir_proyek` date NOT NULL,
  `id_price` int(11) DEFAULT NULL,
  `id_platform` int(11) DEFAULT NULL,
  `id_waktu` int(11) NOT NULL,
  `id_manpower` int(11) NOT NULL,
  `id_enum_status` int(11) NOT NULL DEFAULT '1',
  `net_flow` double(11,3) DEFAULT NULL,
  `criticalpath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_proyek`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of proyek
-- ----------------------------
INSERT INTO `proyek` VALUES ('4', 'Aaaku', 'efdsfsdfsd', 'efsdfsd', '43543543', '40000000', '2017-05-20', '2017-06-03', '1', '1', '1', '2', '2', '0.000', null);
INSERT INTO `proyek` VALUES ('5', 'B', 'Test', 'Test', '878', '120000000', '2017-05-20', '2017-06-10', '3', '2', '1', '4', '3', '0.417', null);
INSERT INTO `proyek` VALUES ('6', 'C', 'Maung', 'Kandang', '80808', '15000000', '2017-05-25', '2017-06-08', '1', '3', '1', '4', '2', '-0.167', null);
INSERT INTO `proyek` VALUES ('7', 'Simamaung', 'Maung', 'Test', '2147483647', '250000000', '2017-07-01', '2017-07-31', '3', '3', '1', '1', '2', '-0.250', null);

-- ----------------------------
-- Table structure for proyek_rao
-- ----------------------------
DROP TABLE IF EXISTS `proyek_rao`;
CREATE TABLE `proyek_rao` (
  `id_proyek` int(11) DEFAULT NULL,
  `id_rao` int(11) DEFAULT NULL,
  `id_proyek_rao` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_proyek_rao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of proyek_rao
-- ----------------------------

-- ----------------------------
-- Table structure for proyek_resiko
-- ----------------------------
DROP TABLE IF EXISTS `proyek_resiko`;
CREATE TABLE `proyek_resiko` (
  `id_proyek_resiko` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyek` int(11) DEFAULT NULL,
  `id_resiko` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_proyek_resiko`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of proyek_resiko
-- ----------------------------
INSERT INTO `proyek_resiko` VALUES ('10', '5', '5');
INSERT INTO `proyek_resiko` VALUES ('11', '5', '6');
INSERT INTO `proyek_resiko` VALUES ('12', '7', '1');
INSERT INTO `proyek_resiko` VALUES ('13', '7', '4');
INSERT INTO `proyek_resiko` VALUES ('14', '7', '6');
INSERT INTO `proyek_resiko` VALUES ('15', '7', '8');
INSERT INTO `proyek_resiko` VALUES ('26', '4', '1');
INSERT INTO `proyek_resiko` VALUES ('27', '4', '4');
INSERT INTO `proyek_resiko` VALUES ('28', '4', '7');
INSERT INTO `proyek_resiko` VALUES ('29', '4', '11');
INSERT INTO `proyek_resiko` VALUES ('30', '4', '12');
INSERT INTO `proyek_resiko` VALUES ('31', '4', '13');
INSERT INTO `proyek_resiko` VALUES ('32', '7', '14');
INSERT INTO `proyek_resiko` VALUES ('33', '7', '15');

-- ----------------------------
-- Table structure for rao
-- ----------------------------
DROP TABLE IF EXISTS `rao`;
CREATE TABLE `rao` (
  `pajak` double DEFAULT NULL,
  `komisi` double DEFAULT NULL,
  `operasional` double DEFAULT NULL,
  `produksi` double DEFAULT NULL,
  `id_rao` int(11) NOT NULL AUTO_INCREMENT,
  `profit` double DEFAULT NULL,
  PRIMARY KEY (`id_rao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rao
-- ----------------------------

-- ----------------------------
-- Table structure for resiko
-- ----------------------------
DROP TABLE IF EXISTS `resiko`;
CREATE TABLE `resiko` (
  `id_resiko` int(255) NOT NULL AUTO_INCREMENT,
  `kode_resiko` varchar(5) DEFAULT NULL,
  `jenis_resiko` varchar(50) DEFAULT NULL,
  `deskripsi_resiko` varchar(255) DEFAULT NULL,
  `probabilitas` double DEFAULT NULL,
  `dampak` double DEFAULT NULL,
  `tingkat_kepentingan_resiko` double DEFAULT NULL,
  `tindakan_pengendali` text,
  PRIMARY KEY (`id_resiko`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of resiko
-- ----------------------------
INSERT INTO `resiko` VALUES ('1', 'R1', 'Kebutuhan', 'test husadhasuid iasdjosiadhysa usahdisa\r\ndjskhadhsauidsa sadhsauidhsadsa\r\nhsduaidhsauid saudhasuidhsaduis dasuidhysauidhsa uiashduisa bdsauhiasd', '0.1', '0.9', '0.9', null);
INSERT INTO `resiko` VALUES ('4', 'R2', 'Kebutuhan', 'asdsad', '0.9', '0.8', '0.72', 'etst');
INSERT INTO `resiko` VALUES ('5', 'R3', 'Tools dan Teknologi', 'wrwrwrw', null, null, null, null);
INSERT INTO `resiko` VALUES ('6', 'R4', 'Kebutuhan', 'kebutuhan apa aja', null, null, null, null);
INSERT INTO `resiko` VALUES ('7', 'R5', 'Tools dan Teknologi', 'dasdsadas', '0.3', '0.3', '0.09', '1. fshfoshfs\r\n2. a;sads\r\n3. sfkjsfsv');
INSERT INTO `resiko` VALUES ('8', 'R6', 'Estimasi', 'wrwrwr3', '0.5', '0.7', '0.35', '1. sdfsef\r\n2. fewd\r\n3. adasdasda');
INSERT INTO `resiko` VALUES ('9', 'R7', 'Kebutuhan', 'test', '0.8', '0.3', '0.24', '1. asdjadhui\r\n2. jskdhdusifhs');
INSERT INTO `resiko` VALUES ('10', 'R8', 'Kebutuhan', 'test2', '0.2', '0.3', '0.06', 'test');
INSERT INTO `resiko` VALUES ('11', 'R9', 'Personal', 'tidak punya itegritas', '0.3', '0.9', '0.27', 'training tiap hari');
INSERT INTO `resiko` VALUES ('12', 'R10', 'Estimasi', 'pengerjaan lebih dari waktu yang ditentukan', '0.3', '0.7', '0.21', '1. mengadakan meeting setiap pagi\r\n2. mengadakan meeting setiap sore');
INSERT INTO `resiko` VALUES ('13', 'R10', 'External', 'xxx', '0.1', '0.3', '0.03', 'asarewssd');
INSERT INTO `resiko` VALUES ('14', 'R10', 'Kebutuhan', 'sdad', '0.3', '0.3', '0.09', 'safsvvfdgg');
INSERT INTO `resiko` VALUES ('15', 'R10', 'Personal', 'gdsgvdgd', '0.1', '0.3', '0.03', 'fsdfdgdf');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(255) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(255) NOT NULL,
  `alamat_user` varchar(255) NOT NULL,
  `telp_user` int(255) NOT NULL,
  `email_user` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_access` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'test', '123', 'test@test.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1');
INSERT INTO `user` VALUES ('2', 'Nama', 'Jl. Unpar 3', '81', 'email@example.com', 'user', 'user', '2');
INSERT INTO `user` VALUES ('3', 'Nama', 'Alamat', '3456789', 'email@example.com', 'backdoor', 'f3fda86e428ccda3e33d207217665201', '1');
INSERT INTO `user` VALUES ('4', 'user', 'user', '80', 'email@example.com', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', '2');

-- ----------------------------
-- Table structure for waktu
-- ----------------------------
DROP TABLE IF EXISTS `waktu`;
CREATE TABLE `waktu` (
  `id_waktu` int(2) NOT NULL AUTO_INCREMENT,
  `kriteria_waktu` varchar(50) NOT NULL,
  `bobot_waktu` int(2) NOT NULL,
  `min_waktu` int(5) DEFAULT NULL,
  `max_waktu` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_waktu`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of waktu
-- ----------------------------
INSERT INTO `waktu` VALUES ('1', '<=2 Bulan', '4', '1', '60');
INSERT INTO `waktu` VALUES ('2', '> 2 Bulan - 4 Bulan', '3', '61', '120');
INSERT INTO `waktu` VALUES ('3', '> 4 Bulan - 6 Bulan', '2', '121', '180');
INSERT INTO `waktu` VALUES ('4', '> 6 Bulan', '1', '181', '99999');
SET FOREIGN_KEY_CHECKS=1;
