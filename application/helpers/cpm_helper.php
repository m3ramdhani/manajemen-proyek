<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

function checkActivity($list, $id, $i)
{
	for ($j=0; $j < $i; $j++) { 
		if ($list[$j]['id'] == $id)
			return $list[$j];
	}
	return null;
}

function getIndex($list, $aux, $i)
{
	for ($j=1; $j < $i; $j++) { 
		if ($list[$j]['id'] == $aux['id'])
			return $j;
	}
	return 0;
}

function setSuccessors($aux, $activity)
{
	if (isset($aux['successors'])) {
		$aux2 = array();
		$aux2['successors'] = $aux['successors'];
		$aux2['successors'][sizeof($aux['successors'])] = $activity;
		$aux['successors'] = $aux2['successors'];
	} else {
		$aux['successors'][0] = $activity;
	}
	return $aux;
}

function get_predecessor($id)
{
	$CI =& get_instance();
    $CI->load->model('m_jadwal');
    $data = $CI->m_jadwal->get_by_id($id)->result();
    if($data){
        return $data[0]->code;
    }
    return "-";
}