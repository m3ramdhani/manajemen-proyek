<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

function convert_rupiah($value){
    if($value==0){
      return  $value = 'Rp '.$value; 
    }else{
      return  $value = 'Rp '. number_format($value, 0);
    }
}

function harikerja($tgl_awal, $tgl_akhir){
	$awal_tgl = strtotime($tgl_awal);
	$akhir_tgl = strtotime($tgl_akhir);

	$waktu_temp = $awal_tgl;
	$working_days = 0;
	while($waktu_temp <= $akhir_tgl) {
		$hari_temp = date('D' , $waktu_temp);
		if (!($hari_temp == 'Sun') && !($hari_temp == 'Sat')) {
			$hari_temp = date('d', $waktu_temp);
			$working_days++;
		}
		$waktu_temp = strtotime('+1 day', $waktu_temp);
	}
	return $working_days;
}

function kepentingan($tingkat_kepentingan_resiko)
{
	$kepentingan = '';
	if ($tingkat_kepentingan_resiko >0.7) {
		$kepentingan = 'Tinggi';
	} else if ($tingkat_kepentingan_resiko >0.3 && $tingkat_kepentingan_resiko <= 0.7) {
		$kepentingan = 'Sedang';
	} else {
		$kepentingan = 'Rendah';
	}

	return $kepentingan;
}

function convert_value($count)
{
	$value = 'Tidak';
	if ($count > 0) {
		$value = 'Ya';
	}
	return $value;
}

function running_action($status)
{
	$value = 'Running';
	if ($status == 3) {
		$value = 'Laporan';
	}
	return $value;
}