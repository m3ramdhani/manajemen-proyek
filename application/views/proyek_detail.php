<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Detail Proyek</h1>
            </div>
        </div>
        <div class="row">
        <?php foreach($proyek as $u){ ?>
            <form action="<?php echo base_url('proyek/update')?>" method="post">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Informasi Dasar
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Nama Proyek</label>
                                <input class="form-control" placeholder="Nama Proyek" name="nama_proyek" value="<?php echo $u->nama_proyek; ?>" readonly>
                            </div>                                    
                            <div class="form-group">
                                <label>PIC Proyek</label>
                                <input class="form-control" placeholder="PIC Proyek" name="pic_proyek"value="<?php echo $u->pic_proyek; ?>" readonly>
                            </div> 
                            <div class="form-group">
                                <label>Alamat</label>
                                <input class="form-control" placeholder="Alamat" name="alamat_proyek" value="<?php echo $u->alamat_proyek; ?>" readonly>
                            </div> 
                            <div class="form-group">
                                <label>Telp</label>
                                <input class="form-control" placeholder="Telp" name="telp_proyek" value="<?php echo $u->telp_proyek; ?>" readonly>
                            </div> 
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Informasi
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Jenis Platform</label>
                                <select class="form-control" name="platform_list" disabled>
                                    <?php 
                                        foreach ($platform as $row) {
                                    ?>
                                        <option value="<?php echo $row->id_platform; ?>" <?php echo ($row->id_platform == $u->id_platform) ? 'selected' : ''; ?>><?php echo $row->kriteria_platform; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <label>Proyek Mulai</label>
                            <div class="form-group input-group" data-provide="datepicker">                                
                                <input type="text" class="form-control mulai_proyek" placeholder="Proyek Mulai" name="mulai_proyek" value="<?php echo $u->mulai_proyek; ?>" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" ><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                            <label>Proyek Berakhir</label>
                            <div class="form-group input-group" data-provide="datepicker">                                
                                <input type="text" class="form-control berakhir_proyek" placeholder="Proyek Berakhir" name="berakhir_proyek" value="<?php echo $u->berakhir_proyek; ?>" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" ><i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
                                <input class="form-control" placeholder="Harga" name="harga_proyek" value="<?php echo number_format($u->harga_proyek); ?>" readonly>
                             </div>
                            <div class="form-group">
                                <label>Manpower</label>
                                <select class="form-control" name="manpower_list" disabled>
                                    <?php 
                                        foreach ($manpower as $row) {
                                    ?>
                                    <option value="<?php echo $row->id_manpower; ?>" <?php echo ($row->id_manpower == $u->id_manpower) ? 'selected' : ''; ?>><?php echo $row->kriteria_manpower; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("proyek/index");?>'">Kembali</button>
                </div>
            </form>
            <?php } ?>
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.mulai_proyek').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });

            $('.berakhir_proyek').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });
        });
    </script>
</body>
</html>
