<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PT. Pilar Timur Teknologi</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>/asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>/asset/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Bootstrap datepicker CSS-->
    <link href="<?php echo base_url();?>/asset/vendor/bootstrap/css/datepicker.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>/asset/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>/asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="<?php echo base_url();?>asset/icon/icon2.png" type="image/gif">
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('dashboard') ?>">PT. Pilar Timur Teknologi</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="<?php echo base_url('login/process_logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                <li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                        
                        <li>
                            <a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard fa-fw"></i> Home</a>
                        </li>
                        <?php if ($this->session->userdata('id_access') == 1) { ?>
                        <li>
                            <a href="#"><i class="fa fa-folder fa-fw"></i> Master Promethee <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('platform') ?>">Platform</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('price') ?>">Price</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('waktu') ?>">Waktu</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('manpower') ?>">Manpower</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        <li>
                            <a href="<?php echo base_url('proyek') ?>"><i class="fa fa-laptop fa-fw"></i> Proyek </a>
                        </li>
                        <?php if ($this->session->userdata('id_access') == 1) { ?>
                        <li>
                            <a href="<?php echo base_url('user') ?>"><i class="fa fa-user fa-fw"></i> User </a>
                        </li>
                        <?php }?>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url();?>/asset/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>/asset/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Bootstrap Core JavaScript Datepicker -->
    <script src="<?php echo base_url();?>/asset/vendor/bootstrap/js/bootstrap-datepicker.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>/asset/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>/asset/dist/js/sb-admin-2.js"></script>

</body>

</html>
