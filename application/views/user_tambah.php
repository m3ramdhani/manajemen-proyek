<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tambah User</h1>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-lg-4">                         
                            <form action="<?php echo base_url('user/tambah/')?>" method="post">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" placeholder="Nama" name="nama_user">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea class="form-control" rows="2" placeholder="Jl. Unpar 3" name="alamat_user"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Telp</label>
                                    <input class="form-control" placeholder="081xxx" name="telp_user">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" placeholder="email@example.com" name="email_user">
                                </div>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" placeholder="Username" name="username">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" placeholder="Password" name="password" type="password">
                                </div>
                                <input type="submit" class="btn btn-success" value="Simpan"> 
                                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("user/index");?>'">Kembali
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

</body>

</html>
