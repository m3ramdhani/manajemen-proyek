<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Resiko</h1>
                </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-lg-4"> 
                        <?php foreach($resiko as $u){ ?>                        
                            <form action="<?php echo base_url('resiko/update').'/'.$u->id_resiko;?>" method="post">
                                <div class="form-group">
                                    <label>Jenis Resiko</label>
                                    <select class="form-control" name="jenis_resiko" value="<?php echo $u->jenis_resiko; ?>">
                                        <option>Kebutuhan</option>
                                        <option>Estimasi</option>
                                        <option>Personal</option>
                                        <option>Tools dan Teknologi</option>
                                        <option>External</option>
                                    </select>
                                </div>                                   
                                <div class="form-group">
                                    <label>Deskripsi Resiko</label>
                                    <textarea class="form-control" rows="3" name="deskripsi_resiko"><?php echo $u->deskripsi_resiko; ?></textarea>
                                </div> 
                               <div class="form-group">
                                    <label>Probabilitas</label>
                                    <input class="form-control" type="text" name="probabilitas" value="<?php echo $u->probabilitas; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label>Dampak</label>
                                    <input class="form-control" type="text" name="dampak" value="<?php echo $u->dampak; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label>Tindakan Pengendali</label>
                                    <textarea class="form-control" rows="3" name="tindakan_pengendali"><?php echo $u->tindakan_pengendali; ?></textarea>
                                </div>                                      
                                <input type="submit" class="btn btn-success" value="Simpan"> 
                                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("resiko/index");?>'">Kembali
                            </button>
                            </form>
                        <?php } ?>
                        </div>
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
</body>
</html>
