<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Laporan Proyek</h1>
                </div>
                    <div class="col-lg-12">
                        <div class="well well-sm">
                            <h4>Nama Proyek  : <?php echo $data_proyek[0]->nama_proyek; ?></h4>
                            <h4>Proyek Dimulai  : <?php echo $data_proyek[0]->mulai_proyek; ?></h4>
                            <h4>Proyek Selesai  : <?php echo $data_proyek[0]->berakhir_proyek; ?></h4>
                            <?php if ($data_proyek[0]->criticalpath != null): ?>
                            <h4>Jalur Kritis : <?php echo $data_proyek[0]->criticalpath; ?></h4>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="col-md-offset-10">
                        <a href="<?php echo $url; ?>" class="btn btn-md btn-success">Tambah</a>
                        <button type="button" class="btn btn-danger" onclick="window.location='<?php echo $back_url;?>'">Kembali</button>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>                            
                                <tr>
                                    <th class="text-center">Tanggal</th>
                                    <th class="text-center">Resiko Terjadi</th>
                                    <th class="text-center">Deskripsi Laporan</th>                                    
                                    <th class="text-center">Aksi</th>                                     
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach ($laporan as $row) {
                            ?>
                                <tr>
                                    <td class="text_center"><?php echo $row->tanggal_laporan; ?></td>
                                    <td class="text_center"><?php echo convert_value($row->jml_resiko); ?></td>
                                    <td class="text_center"><?php echo $row->deskripsi_laporan; ?></td>
                                    <td class="text_center"><a href="<?php echo base_url() ?>proyek/detail_laporan/<?php echo $row->id_laporan; ?>" class="btn btn-default" <?php echo ($row->jml_resiko <= 0) ? 'disabled' : ''; ?>><i class="glyphicon glyphicon-search"></i></a></td>
                                </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>


</html>
