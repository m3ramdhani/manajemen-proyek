<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Laporan</h1>
                </div>                  
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-lg-8">                      
                            <form action="<?php echo $url;?>" method="post">
                            <div class="form-group">
                                <label> Nama Proyek</label>
                                <input type="text" class="form-control" rows="5" name="nama_proyek" value="<?php echo $proyek->nama_proyek; ?>" readonly></input>
                            </div>
                            <label> Tanggal Laporan</label>
                            <div class="form-group input-group" data-provide="datepicker">                                
                                <input type="datepicker" class="form-control tanggal_laporan" name="tanggal_laporan">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" ><i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>                                                                
                            <div class="form-group">
                                <label form="resiko" class="control-label">Apakah Terjadi Resiko?</label>
                                <div class="radio">                                
                                    <label class="radio-inline text-left">
                                        <?php echo form_radio($resiko_yes); ?>
                                        <small>Ya</small>
                                    </label>
                                    <label class="radio-inline">
                                        <?php echo form_radio($resiko_no); ?>
                                        <small>Tidak</small>
                                    </label>                                    
                                </div>
                            </div>
                            <div class="list-resiko form-group align-left" style="display:none;">
                                <div class="align-left">
                                    <label>Daftar Resiko</label>
                                </div>
                                <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Check</th>
                                        <th class="text-center">Kode</th>
                                        <th class="text-center">Deskripsi Resiko</th>                                   
                                    </tr>
                                </thead>
                                    <tbody>
                                    <?php
                                        foreach ($list_resiko as $row) {
                                    ?>
                                        <tr>
                                            <td class="text-center"><input type="checkbox" name="resiko[]" value="<?php echo $row->id_resiko; ?>"/></td>
                                            <td><?php echo $row->kode_resiko; ?></td>
                                            <td><?php echo $row->deskripsi_resiko; ?></td>
                                        </tr>
                                    <?php
                                        }
                                    ?>  
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi Laporan</label>
                                <textarea class="form-control" rows="5" name="deskripsi_laporan"></textarea>
                            </div>
                                <input type="submit" class="btn btn-success" value="Simpan"> 
                                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo $back_url;?>'">Kembali
                            </button>
                            </form>
                        </div>
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.tanggal_laporan').datepicker({
                sideBySide: true,
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $('#resiko_yes').on('click', function(){
                $('.list-resiko ').show();
            });
            $('#resiko_no').on('click', function(){
                $('.list-resiko ').hide();
            });
        });
    </script>
</body>
</html>
