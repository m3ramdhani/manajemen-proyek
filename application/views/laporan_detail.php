<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Detail Laporan</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="well well-sm">
                    <h4>Tanggal  : <?php echo $laporan->tanggal_laporan; ?></h4>
                    <h4>Deskripsi  : <?php echo $laporan->deskripsi_laporan; ?></h4>
                </div>
            </div>
            <table width="100%" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Kode Resiko</th>
                        <th class="text-center">Deskripsi Resiko</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($detail as $row) {
                ?>
                    <tr>
                        <td><?php echo $row->kode_resiko; ?></td>
                        <td><?php echo $row->deskripsi_resiko; ?></td>
                    </tr>
                <?php 
                    }
                ?>
                </tbody>
            </table>
            <div class="col-lg-4">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo $back_url;?>'">Kembali</button>
            </div>
        </div>
        <!-- /.row -->
    </div>
</body>
</html>
