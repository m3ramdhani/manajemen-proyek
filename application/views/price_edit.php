<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Price</h1>
                    </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-lg-4"> 
                            <?php foreach($price as $u){ ?>                        
                                <form action="<?php echo base_url('price/update').'/'.$u->id_price;?>" method="post">
                                    <div class="form-group">
                                        <label>Kriteria</label>
                                        <input class="form-control" placeholder="Kriteria" name="kriteria_price" value="<?php echo $u->kriteria_price; ?>">
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Bobot Penilaian</label>
                                        <input class="form-control" placeholder="Bobot Penilaian" name="bobot_price" value="<?php echo $u->bobot_price; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Min Price</label>
                                        <input class="form-control" placeholder="Min Price" name="min_price" value="<?php echo $u->min_price; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Max Price</label>
                                        <input class="form-control" placeholder="Min Price" name="max_price" value="<?php echo $u->max_price; ?>">
                                    </div>                                     
                                    <input type="submit" class="btn btn-success" value="Simpan"> 
                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("price/index");?>'">Kembali
                                </button>
                                </form>
                            <?php } ?>
                            </div>
                        </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

</body>

</html>
