<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Resiko Proyek</h1>
                </div>               
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <div class="well well-sm">
                                <h4>Nama Proyek  : <?php echo $data_proyek[0]->nama_proyek; ?></h4>
                                <h4>Proyek Dimulai  : <?php echo $data_proyek[0]->mulai_proyek; ?></h4>
                                <h4>Proyek Selesai  : <?php echo $data_proyek[0]->berakhir_proyek; ?></h4>
                            </div>
                        </div>
                        <!-- /.col-lg-4 -->
                        <?php
                            if ($data_proyek[0]->id_enum_status != 4) {
                        ?>
                        <div class="col-md-offset-10">
                            <button type="button" class="btn btn-success" onclick="window.location='<?php echo base_url().'resiko/tambah/'.$data_proyek[0]->id_proyek; ?>'">Tambah</button>
                            <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("proyek/index");?>'">Kembali</button>
                        </div>
                        <?php
                            }
                        ?>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-resiko">
                                <thead>
                                    <tr>
                                        <th class="text-center">Kode Resiko</wth>
                                        <th class="text-center">Jenis Resiko</th>
                                        <th class="text-center">Deskripsi Resiko</th>
                                        <th class="text-center">Tingkat Kepentingan</th>
                                        <th class="text-center">Tindakan pengendali</th>
                                        <?php
                                            if ($data_proyek[0]->id_enum_status != 4) {
                                        ?>
                                        <th class="text-center">Aksi</th>
                                        <?php
                                            }
                                        ?>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (empty($resiko_proyek)) { ?>
                                        <tr>
                                            <td class="text-center" colspan="6">Data Tidak Ditemukan.</td>
                                        </tr>
                                    <?php } else {
                                        foreach ($resiko_proyek as $value) { ?>
                                        <tr class="odd gradeX">
                                            <td class="text-center" width="5%"><?php echo $value->kode_resiko; ?></td>
                                            <td class="text-center" width="10%"><?php echo $value->jenis_resiko; ?></td>
                                            <td class="" width="35%;"><?php echo $value->deskripsi_resiko; ?></td>
                                            <td class="text-center" width="10%"><?php echo kepentingan($value->tingkat_kepentingan_resiko); ?></td>
                                            <td class="" width="25%"><?php echo $value->tindakan_pengendali; ?></td>
                                            <?php
                                                if ($data_proyek[0]->id_enum_status != 4) {
                                            ?>
                                            <td class="text-center" width="15%">
                                                <a href="<?php echo base_url() ?>resiko/update/<?php echo $value->id_resiko ?>" class="btn btn-info btn-sm">Edit</i></a>
                                                <a href="<?php echo base_url() ?>resiko/delete/<?php echo $value->id_resiko ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda Yakin menghapus <?php echo $value->kode_resiko ?> ?')">Hapus </a>
                                            </td>
                                            <?php
                                                }
                                            ?>
                                        </tr>
                                    <?php }
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>


    <script type="text/javascript">
        
    </script>
</body>

</html>
