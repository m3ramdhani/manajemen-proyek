<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Jadwal Proyek</h1>
                </div>               
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <div class="well well-sm">
                                <h4>Nama Proyek  : <?php echo $data_proyek[0]->nama_proyek; ?></h4>
                                <h4>Proyek Dimulai  : <?php echo $data_proyek[0]->mulai_proyek; ?></h4>
                                <h4>Proyek Selesai  : <?php echo $data_proyek[0]->berakhir_proyek; ?></h4>
                                <?php if ($data_proyek[0]->criticalpath != null): ?>
                                <h4>Jalur Kritis : <?php echo $data_proyek[0]->criticalpath; ?></h4>
                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- /.col-lg-4 -->
                        
                        <input type="hidden" name="mulai-proyek" value="<?php echo $data_proyek[0]->mulai_proyek; ?>">
                        <input type="hidden" name="akhir-proyek" value="<?php echo $data_proyek[0]->berakhir_proyek; ?>">
                        <input type="hidden" name="id_proyek" value="<?php echo $data_proyek[0]->id_proyek; ?>">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th class="text-center">Nama Pekerjaan</th>
                                    <th class="text-center">Mulai</th>
                                    <th class="text-center">Selesai</th>
                                    <th class="text-center">Kode Kegiatan</th>
                                    <th class="text-center">Kegiatan Pendahulu</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                                
                            </thead>
                            <tbody>
                            <?php
                                if (!empty($tasklist)) {
                                    foreach ($tasklist as $row) {
                            ?>
                                        <tr>
                                            <td class="text-center"><?php echo $row->tasklist; ?></td>
                                            <td class="text-center"><?php echo $row->start_date; ?></td>
                                            <td class="text-center"><?php echo $row->end_date; ?></td>
                                            <td class="text-center"><?php echo $row->code; ?></td>
                                            <td class="text-center">
                                            <?php
                                                $list_predecessor = explode(",", $row->predecessor);
                                                $get_predecessor = array();
                                                if (count($list_predecessor) > 1) {
                                                    $i = 0;
                                                    foreach ($list_predecessor as $value) {
                                                        $get_predecessor[$i] = get_predecessor($value);
                                                        $i++;
                                                    }
                                                    $predecessor = implode(", ", $get_predecessor);
                                                    echo $predecessor;
                                                } else {
                                                    echo get_predecessor($row->predecessor);
                                                }
                                            ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?php echo base_url() ?>jadwal/delete/<?php echo $row->id_jadwal; ?>/<?php echo $row->id_proyek; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda Yakin menghapus data jadwal ?')"><i class="glyphicon glyphicon-trash"></i></a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                }
                            ?>
                                <form action="<?php echo $add_url; ?>" method="post">
                                    <tr>                                    
                                        <th class="text-center"><input type="text" name="tasklist" class="form-control" placeholder="Masukkan nama pekerjaan"></th>
                                        <th class="text-center">
                                            <div class="form-group input-group">
                                                <input id="persiapan-awal" type="datepicker" class="form-control persiapan-awal" name="start_date">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </th>
                                        <th class="text-center">
                                            <div class="form-group input-group">
                                                <input id="persiapan-akhir" type="datepicker" class="form-control persiapan-akhir" name="end_date">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </th>
                                        <th class="text-center"><input type="text" name="code" class="form-control" placeholder="Masukkan kode kegiatan"></th>
                                        <th class="text-center">
                                            <select name="predecessor[]" class="form-control" multiple="" size="5">
                                            <?php
                                                foreach ($tasklist as $row) {
                                                    echo '<option value="'.$row->id_jadwal.'">'.$row->tasklist.'</option>';
                                                }
                                            ?>
                                            </select>
                                        </th>
                                        <th class="text-center"><input type="submit" class="btn btn-primary" value="Tambah"></th>
                                    </tr>
                                </form>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-success" onclick="window.location='<?php echo $url_hitung;?>'">Hitung</button>
                        <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("proyek");?>'">Kembali</button>
                        
                        <!-- /.table-responsive -->
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            $('.persiapan-awal').datepicker({
                sideBySide: true,
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $('.persiapan-akhir').datepicker({
                sideBySide: true,
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $(".persiapan-awal").on("dp.change", function (e) {
                $('.persiapan-akhir').datepicker({
                    sideBySide: true,
                    format: 'yyyy-mm-dd' 
                });                
                $('.persiapan-akhir').data("DateTimePicker").minDate(e.date);
            });

            $('.analisis-awal').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $('.analisis-akhir').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });

            $('.development-awal').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $('.development-akhir').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });

            $('.backend-awal').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $('.backend-akhir').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });

            $('.qa-awal').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $('.qa-akhir').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });

            $('.support-awal').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });
            $('.support-akhir').datepicker({
                format: "yyyy-mm-dd",
                autoclose:true
            });
        });
    </script>
</body>

</html>
