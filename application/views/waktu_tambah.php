<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tambah Waktu</h1>
                    </div>                  
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-lg-4">                      
                                <form action="<?php echo base_url('waktu/tambah');?>" method="post">
                                    <div class="form-group">
                                        <label>Kriteria</label>
                                        <input class="form-control" placeholder="Kriteria" name="kriteria_waktu">
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Bobot Penilaian</label>
                                        <input class="form-control" placeholder="Bobot Penilaian" name="bobot_waktu">
                                    </div>
                                    <div class="form-group">
                                        <label>Min Waktu</label>
                                        <input class="form-control" placeholder="Min Waktu" name="min_waktu">
                                    </div>
                                    <div class="form-group">
                                        <label>Max Waktu</label>
                                        <input class="form-control" placeholder="Max Waktu" name="max_waktu">
                                    </div>                                     
                                    <input type="submit" class="btn btn-success" value="Simpan"> 
                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("waktu/index");?>'">Kembali
                                </button>
                                </form>
                            </div>
                        </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

</body>

</html>
