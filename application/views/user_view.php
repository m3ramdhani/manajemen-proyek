<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">List User</h1>
                    </div>
                    <div class="col-md-offset-11">
                        <a href="<?php echo base_url() ?>user/tambah/" class="btn btn-sm btn-success">Tambah</a>
                    </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>                                    
                                    <tr>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Telp</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if (empty($data)) {?>
                                    <tr>
                                        <td> Data Tidak Ditemukan </td>
                                    </tr>
                                <?php
                                } else {
                                        foreach($data->result() as $row) { ?>
                                        <tr class="odd gradeX" >                                    
                                            <td class="text-center"><?php echo $row->nama_user; ?></td>
                                            <td class="text-center"><?php echo $row->alamat_user; ?></td>
                                            <td class="text-center"><?php echo $row->telp_user; ?></td>
                                            <td class="text-center"><?php echo $row->email_user; ?></td>
                                            <td class="text-center">
                                                <a href="<?php echo base_url() ?>user/detail/<?php echo $row->id_user ?>" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></a>
                                                <a href="<?php echo base_url() ?>user/update/<?php echo $row->id_user ?>" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>
                                                <a href="<?php echo base_url() ?>user/delete/<?php echo $row->id_user ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda Yakin menghapus <?php echo $row->nama_user ?> ?')"><i class="glyphicon glyphicon-trash"></i></a>
                                            </td>                                                                    
                                        </tr>
                                        <?php 
                                    }
                                } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>


</body>

</html>
