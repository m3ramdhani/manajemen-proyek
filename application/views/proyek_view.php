<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">List Proyek</h2>
                </div>
                <div>
                    <div class="col-md-offset-9">
                        <input id="base_url" type="hidden" value="<?php echo base_url(); ?>"/>
                        <button type="button" class="btn btn-danger btn-run-promethee" url="<?php echo site_url("promethee/run");?>">Hitung Prioritas</button>
                        <button type="button" class="btn btn-success" onclick="window.location='<?php echo site_url("proyek/tambah");?>'">Tambah</button>
                    </div>
                </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th class="text-center">Nama Proyek</th>
                                    <th class="text-center">Jenis</th>
                                    <th class="text-center">Mulai</th>
                                    <th class="text-center">Selesai</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Data</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($data_proyek as $key) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $key->nama_proyek; ?></td>
                                    <td><?php echo $key->kriteria_platform; ?></td>
                                    <td><?php echo $key->mulai_proyek; ?></td>
                                    <td class="text-center"><?php echo $key->berakhir_proyek; ?></td>
                                    <td class="text-center"><?php echo $key->nama_status; ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url().'jadwal/detail/'.$key->id_proyek; ?>" class="btn btn-md btn-info">Jadwal</a>
                                        <a href="<?php echo base_url().'proyek/resiko/'.$key->id_proyek; ?>" class="btn btn-md btn-default">Resiko</a>
                                        <a href="<?php echo base_url().'rao/detail/'.$key->id_proyek; ?>" class="btn btn-md btn-success">RAO</a>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url() ?>proyek/detail/<?php echo $key->id_proyek ?>" class="btn btn-default btn-md">Detail</i></a>
                                    <?php
                                        if ($key->id_enum_status == 3) {
                                    ?>                                        
                                        <a href="<?php echo base_url() ?>proyek/update/<?php echo $key->id_proyek ?>" class="btn btn-info btn-md">Edit</i></a>
                                        <a href="<?php echo base_url().'proyek/laporan/'.$key->id_proyek; ?>" class="btn btn-md btn-danger"><?php echo running_action($key->id_enum_status); ?></a>
                                        <a href="<?php echo base_url() ?>proyek/done/<?php echo $key->id_proyek ?>" class="btn btn-success btn-md" onclick="return confirm('Apakah proyek ini sudah selesai ?')">Done </a>
                                    <?php
                                        } else if ($key->id_enum_status == 2) {
                                    ?>
                                        <a href="<?php echo base_url() ?>proyek/update/<?php echo $key->id_proyek ?>" class="btn btn-info btn-md">Edit</i></a>
                                        <a href="<?php echo base_url() ?>proyek/delete/<?php echo $key->id_proyek ?>" class="btn btn-danger btn-md" onclick="return confirm('Anda Yakin menghapus <?php echo $key->nama_proyek ?> ?')">Hapus </a>
                                    <?php
                                        }
                                    ?>
                                    </td>
                                </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                </div>
            </div>
            <div id="result" class="row" style="<?php echo (sizeof($proyek_prioritas) > 0 ? '' : 'display:none;'); ?>">
                <div class="col-lg-12">
                    <h2 class="page-header">Prioritas Proyek</h2>
                </div>
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-prioritas">
                        <thead>
                            <tr>
                                <th class="text-center">Prioritas Ke-</th>
                                <th class="text-center">Nama Proyek</th>
                                <th class="text-center">Jenis</th>
                                <th class="text-center">Mulai</th>
                                <th class="text-center">Selesai</th>                                
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i = 1;
                                if (sizeof($proyek_prioritas) > 0) {
                                    foreach ($proyek_prioritas as $key) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $key->nama_proyek; ?></td>
                                <td><?php echo $key->kriteria_platform; ?></td>
                                <td><?php echo $key->mulai_proyek; ?></td>
                                <td><?php echo $key->berakhir_proyek; ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url().'proyek/laporan/'.$key->id_proyek; ?>" class="btn btn-md btn-info"><?php echo running_action($key->id_enum_status); ?></a>
                                </td>
                            </tr>
                            <?php
                                    $i++;
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-run-promethee').click(function(){
                $("#dataTables-prioritas tbody tr").remove();
                base_url = $('#base_url').val();
                $.ajax({
                    url: base_url + "promethee/run",
                    type: "GET",
                    dataType: "JSON",
                    success: function (response) {
                        $("#result").find("#dataTables-prioritas").find('tbody').append(response.data);
                        $("#result").show();
                    }
                });
            });
        });
    </script>
</body>
</html>