<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit User</h1>
                    </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-lg-4">
                            <?php foreach($user as $u){ ?>                         
                                <form action="<?php echo base_url('user/update').'/'.$u->id_user;?>" method="post">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input class="form-control" placeholder="Nama" name="nama_user" value="<?php echo $u->nama_user; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control" name="alamat_user"><?php echo $u->alamat_user; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Telp</label>
                                        <input class="form-control" name="telp_user" placeholder="081xxx" value="<?php echo $u->telp_user; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" name="email_user" placeholder="email@example.com" value="<?php echo $u->email_user; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input class="form-control" name="username" placeholder="Username" value="<?php echo $u->username; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo $u->password; ?>">
                                    </div>
                                    <input type="submit" value="Simpan" class="btn btn-success">
                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("user/index");?>'">Kembali
                                </button>
                                </form>
                                <?php } ?>
                            </div>
                        </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

</body>

</html>
