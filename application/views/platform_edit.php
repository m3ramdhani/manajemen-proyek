<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Platform</h1>
                    </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-lg-4"> 
                            <?php foreach($platform as $u){ ?>                        
                                <form action="<?php echo base_url('platform/update').'/'.$u->id_platform;?>" method="post">
                                    <div class="form-group">
                                        <label>Kriteria</label>
                                        <input class="form-control" placeholder="Kriteria" name="kriteria_platform" value="<?php echo $u->kriteria_platform; ?>">
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Bobot Penilaian</label>
                                        <input class="form-control" placeholder="Bobot Penilaian" name="bobot_platform" value="<?php echo $u->bobot_platform; ?>">
                                    </div>                                    
                                    <input type="submit" class="btn btn-success" value="Simpan"> 
                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("platform/index");?>'">Kembali
                                </button>
                                </form>
                            <?php } ?>
                            </div>
                        </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

</body>

</html>
