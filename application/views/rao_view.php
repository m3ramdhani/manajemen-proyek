<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <?php
        $pajak = 0.13 * $data_proyek->harga_proyek;
        $komisi = 0.2 * $data_proyek->harga_proyek;
        $operasional = 0.05 * $data_proyek->harga_proyek;
        $produksi = 0.4 * $data_proyek->harga_proyek;
        $total = $pajak + $komisi + $operasional + $produksi;
        $profit = $data_proyek->harga_proyek - $total;

        $date1 = date_create($data_proyek->mulai_proyek);
        $date2 = date_create($data_proyek->berakhir_proyek);
        $diff = date_diff($date1, $date2);
        $days = 1 + $diff->format('%a');
        $working_days = harikerja($data_proyek->mulai_proyek, $data_proyek->berakhir_proyek);
        $month = round($working_days / $days);
    ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Rancangan Anggaran Operasional</h1>
                </div>               
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <div class="well well-sm">
                                <h4>Nama Proyek  : <?php echo $data_proyek->nama_proyek; ?></h4>
                                <h4>Anggaran Total Proyek  : <?php echo convert_rupiah($data_proyek->harga_proyek); ?></h4>
                            </div>
                        </div>
                        <!-- /.col-lg-4 -->
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan="4" bgcolor="grey"><h4>Pajak</h4></th>
                                    <th><input type="text" class="form-control text-right" placeholder="Anggaran" name="pajak_anggaran" value="<?php echo number_format($pajak); ?>" readonly></th>
                                </tr>
                                <tr>
                                    <th class="text-center">Jenis</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">Bulan</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">Omset</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.01 * $data_proyek->harga_proyek); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah(0.01 * $data_proyek->harga_proyek); ?></th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">PPH 23</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.02 * $data_proyek->harga_proyek); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah(0.02 * $data_proyek->harga_proyek); ?></th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">PPN</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.1 * $data_proyek->harga_proyek); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah(0.1 * $data_proyek->harga_proyek); ?></th>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="4" bgcolor="grey"><h4>Komisi</h4></th>
                                    <th><input type="text" class="form-control text-right" placeholder="Anggaran" name="komisi" value="<?php echo number_format($komisi); ?>" readonly></th>
                                </tr>
                                <tr>
                                    <th class="text-center">Jenis</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">Bulan</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">Marketing</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.3 * $komisi); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah(0.3 * $komisi); ?></th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">Produksi</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.5 * $komisi); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah(0.5 * $komisi); ?></th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">Corporate</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.2 * $komisi); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah(0.2 * $komisi); ?></th>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="4" bgcolor="grey"><h4>Operasional</h4></th>
                                    <th><input type="text" class="form-control text-right" placeholder="Anggaran" name="operasional" value="<?php echo number_format($operasional); ?>" readonly></th>
                                </tr>
                                <tr>
                                    <th class="text-center">Jenis</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">Bulan</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Komunikasi</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.035 * $operasional); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="<?php echo $month; ?>" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah((0.035 * $operasional) * 1 * $month); ?></th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">Meeting</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.965 * $operasional / 3); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="3" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah((0.965 * $operasional)); ?></th>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="4" bgcolor="grey"><h4>Produksi</h4></th>
                                    <th><input type="text" class="form-control text-right" placeholder="Anggaran" name="produksi" value="<?php echo number_format($produksi); ?>" readonly></th>
                                </tr>
                                <tr>
                                    <th class="text-center">Jenis</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">Bulan</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">Salary Pegawai</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.05 * $produksi); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="<?php echo $data_proyek->manpower; ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="<?php echo $month; ?>" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah((0.05 * $produksi) * $data_proyek->manpower * $month); ?></th>
                                </tr>
                                <tr>                                    
                                    <th class="text-center">Tanggungan Corporate</th>
                                    <th class="text-center"><input type="text" class="form-control text-right" value="<?php echo number_format(0.1 * $produksi); ?>" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="1" readonly></th>
                                    <th class="text-center"><input type="text" class="form-control" value="<?php echo $month; ?>" readonly></th>
                                    <th class="text-right"><?php echo convert_rupiah((0.1 * $produksi) * 1 * $month); ?></th>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="4" bgcolor="grey"><h4>Total Anggaran</h4></th>
                                    <th class="text-right"><?php echo convert_rupiah($total); ?></th>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="4" bgcolor="grey"><h4>Profit</h4></th>
                                    <th class="text-right"><?php echo convert_rupiah($profit); ?></th>
                                </tr>
                                
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                        <div class="col-md-offset-11">
                            <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("proyek/index");?>'">Kembali</button>
                        </div>
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

</body>

</html>
