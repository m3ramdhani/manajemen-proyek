<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Resiko</h1>
                </div>                  
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-lg-4">                      
                            <form action="<?php echo base_url('resiko/tambah');?>" method="post">
                                <div class="form-group">
                                    <label>Jenis Resiko</label>
                                    <select class="form-control" name="jenis_resiko">
                                        <option>Kebutuhan</option>
                                        <option>Estimasi</option>
                                        <option>Personal</option>
                                        <option>Tools dan Teknologi</option>
                                        <option>External</option>
                                    </select>
                                </div>                                   
                                <div class="form-group">
                                    <label>Deskripsi Resiko</label>
                                    <textarea class="form-control" rows="3" name="deskripsi_resiko"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Probabilitas</label>
                                    <input class="form-control" type="text" name="probabilitas" />
                                </div>
                                <div class="form-group">
                                    <label>Dampak</label>
                                    <input class="form-control" type="text" name="dampak" />
                                </div>
                                <div class="form-group">
                                    <label>Tindakan Pengendali</label>
                                    <textarea class="form-control" rows="3" name="tindakan_pengendali"></textarea>
                                </div>
                                <input type="hidden" name="id_proyek" value="<?php echo $this->uri->segment(3); ?>">
                                <input type="submit" class="btn btn-success" value="Simpan"> 
                                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("proyek/resiko/").'/'.$this->uri->segment(3);?>'">Kembali
                            </button>
                            </form>
                        </div>
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
</body>
</html>
