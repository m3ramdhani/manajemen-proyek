<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Price</h1>
                </div>
                <div class="col-md-offset-11">
                        <a href="<?php echo base_url() ?>price/tambah/" class="btn btn-sm btn-success">Tambah</a>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th class="text-center">Kriteria</th>
                                    <th class="text-center">Bobot</th>
                                    <th class="text-center">Min</th>
                                    <th class="text-center">Max</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (empty($price)) {?>
                                    <tr>
                                        <td> Data Tidak Ditemukan </td>
                                    </tr>
                                <?php
                                } else {
                                        foreach($price->result() as $row) { ?>
                                        <tr class="odd gradeX" >                                    
                                            <td class="text-center"><?php echo $row->kriteria_price; ?></td>
                                            <td class="text-center"><?php echo $row->bobot_price; ?></td>
                                            <td class="text-center"><?php echo $row->min_price; ?></td>
                                            <td class="text-center"><?php echo $row->max_price; ?></td>
                                            <td class="text-center">
                                                <a href="<?php echo base_url() ?>price/update/<?php echo $row->id_price ?>" class="btn btn-info btn-sm">Edit</i></a>
                                                <a href="<?php echo base_url() ?>price/delete/<?php echo $row->id_price ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda Yakin menghapus <?php echo $row->kriteria_price ?> ?')">Hapus </a>
                                            </td>                                                                    
                                        </tr>
                                        <?php 
                                    }
                                } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

</body>

</html>
