<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Manpower</h1>
                    </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-lg-4"> 
                            <?php foreach($manpower as $u){ ?>                        
                                <form action="<?php echo base_url('manpower/update').'/'.$u->id_manpower;?>" method="post">
                                    <div class="form-group">
                                        <label>Kriteria</label>
                                        <input class="form-control" placeholder="Kriteria" name="kriteria_manpower" value="<?php echo $u->kriteria_manpower; ?>">
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Bobot Penilaian</label>
                                        <input class="form-control" placeholder="Bobot Penilaian" name="bobot_manpower" value="<?php echo $u->bobot_manpower; ?>">
                                    </div>                                    
                                    <input type="submit" class="btn btn-success" value="Simpan"> 
                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo site_url("manpower/index");?>'">Kembali
                                </button>
                                </form>
                            <?php } ?>
                            </div>
                        </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>


</body>

</html>
