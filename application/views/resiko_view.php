<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('header'); ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Resiko</h1>
                </div>
                  <div class="col-md-offset-11">
                        <a href="<?php echo base_url() ?>resiko/tambah/" class="btn btn-sm btn-success">Tambah</a>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>                            
                                <tr>
                                    <th class="text-center">Kode Resiko</th>
                                    <th class="text-center">Jenis Resiko</th>
                                    <th class="text-center">Deskripsi Resiko</th>                                    
                                    <th class="text-center">Tingkat Kepentingan</th>
                                    <th class="text-center">Tindakan pengendali</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (empty($resiko)) {?>
                                    <tr>
                                        <td> Data Tidak Ditemukan </td>
                                    </tr>
                                <?php
                            } else {
                                foreach($resiko->result() as $row) { ?>
                                <tr class="odd gradeX" >
                                    <td class="text-center"><?php echo $row->kode_resiko; ?></td>                                    
                                    <td class="text-center"><?php echo $row->jenis_resiko; ?></td>
                                    <td><?php echo $row->deskripsi_resiko; ?></td>
                                    <td><?php echo kepentingan($row->tingkat_kepentingan_resiko); ?></td>
                                    <td><?php echo $row->tindakan_pengendali; ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url() ?>resiko/update/<?php echo $row->id_resiko ?>" class="btn btn-info btn-sm">Edit</i></a>
                                        <a href="<?php echo base_url() ?>resiko/delete/<?php echo $row->id_resiko ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda Yakin menghapus <?php echo $row->kode_resiko ?> ?')">Hapus </a>
                                    </td>                                                                    
                                </tr>
                                <?php 
                                    }
                                } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>


</html>
