<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_price extends CI_Model {
	private $primary_key = 'id_price';
	private $table_name = 'price';

	function __construct(){
		parent::__construct();
	}

	function count_all(){
		return $this->db->count_all($this->table_name);
	}

	function get_all_price(){
		return $this->db->get($this->table_name);
	}

	function get_by_id($id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->get($this->table_name);
	}

	function insert($data){
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function update($data, $id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->update($this->table_name, $data);
	}

	function delete($id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->delete($this->table_name);
	}

}

/* End of file m_platfrom.php */
/* Location: ./application/model/m_platfrom.php */