<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_manpower extends CI_Model {
	private $primary_key = 'id_manpower';
	private $table_name = 'manpower';

	function __construct(){
		parent::__construct();
	}

	function count_all(){
		return $this->db->count_all($this->table_name);
	}

	function get_all_manpower(){
		return $this->db->get($this->table_name);
	}

	function get_by_id($id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->get($this->table_name);
	}

	function insert($data){
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function update($data, $id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->update($this->table_name, $data);
	}

	function delete($id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->delete($this->table_name);
	}
}

/* End of file M_manpower.php */
/* Location: ./application/model/M_manpower.php */