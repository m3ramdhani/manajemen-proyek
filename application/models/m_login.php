<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class M_login extends CI_Model
{
	private $table_name = 'user';

	function __construct()
	{
		parent::__construct();
	}

	function check_user($username, $password)
	{
		$query = $this->db->get_where($this->table_name, array('username' => $username, 'password' => $password), 1, 0);

		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}