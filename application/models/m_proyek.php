<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proyek extends CI_Model {
	private $primary_key = 'id_proyek';
	private $table_name = 'proyek';
	private $table_name2 = 'enum_status';
	private $primary_key2 = 'id_status';	

	function __construct(){
		parent::__construct();
	}

	function count_all(){
		return $this->db->count_all($this->table_name);
	}

	function get_all_proyek(){
		return $this->db->get($this->table_name);
	}

	function get_status(){
		return $this->db->get($this->table_name2);
	}
	function get_by_id($id_proyek){
		$this->db->where($this->primary_key, $id_proyek);
		return $this->db->get($this->table_name);
	}

	function insert($data){
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function update($data, $id_proyek){
		$this->db->where($this->primary_key, $id_proyek);
		return $this->db->update($this->table_name, $data);
	}

	function delete($id_proyek){
		$this->db->where($this->primary_key, $id_proyek);
		return $this->db->delete($this->table_name);
	}

	function get_proyek($id = 0, $priority = 0){
		$this->db->select('proyek.id_proyek,nama_proyek, platform.kriteria_platform, mulai_proyek, berakhir_proyek, id_enum_status, enum_status_proyek.nama_status, value as manpower,harga_proyek');
		$this->db->from('proyek');
		$this->db->join('manpower', 'manpower.id_manpower = proyek.id_manpower');
		$this->db->join('enum_status_proyek','enum_status_proyek.id_status = proyek.id_enum_status');
		$this->db->join('platform','platform.id_platform=proyek.id_platform');
		if ($id != 0) {
			$this->db->where('id_proyek', $id);
		}
		if ($priority != 0) {
			$this->db->where_in('id_enum_status', array(2,3));
			$this->db->order_by('net_flow', 'desc');
		}
		return $this->db->get()->result();
	}

	function get_laporan($id_proyek = 0){
		$this->db->select('laporan_proyek.id_laporan, tanggal_laporan, count(id_resiko) as jml_resiko, deskripsi_laporan', false)
			 ->from('laporan_proyek')
			 ->join('detail_laporan', 'detail_laporan.id_laporan = laporan_proyek.id_laporan', 'left')
			 ->where('id_proyek', $id_proyek)
			 ->group_by(array("tanggal_laporan", "deskripsi_laporan"));
		return $this->db->get()->result();
	}
}

/* End of file M_manpower.php */
/* Location: ./application/model/M_manpower.php */