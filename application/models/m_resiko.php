<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_resiko extends CI_Model {
	private $primary_key = 'id_resiko';
	private $table_name = 'resiko';

	function __construct(){
		parent::__construct();
	}

	function count_all(){
		return $this->db->count_all($this->table_name);
	}

	function get_all_resiko(){
		return $this->db->get($this->table_name);
	}

	function get_max_code(){
		$this->db->select_max('kode_resiko');
		return $this->db->get($this->table_name)->result();
	}

	function get_by_id($id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->get($this->table_name);
	}

	function insert($data){
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function update($data, $id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->update($this->table_name, $data);
	}

	function delete($id_user){
		$this->db->where($this->primary_key, $id_user);
		return $this->db->delete($this->table_name);
	}

	function get_resiko_proyek($id = 0)
	{
		$this->db->select('r.*, py.nama_proyek')
				 ->from('proyek_resiko pr')
				 ->join('proyek py', 'py.id_proyek = pr.id_proyek')
				 ->join('resiko r', 'r.id_resiko = pr.id_resiko')
				 ->where('pr.id_proyek', $id);
		return $this->db->get()->result();
	}

	function delete_resiko($id = 0)
	{
		$this->db->where('id_proyek', $id);
		return $this->db->delete('proyek_resiko');
	}

	function add_resiko($data)
	{
		$this->db->insert('proyek_resiko', $data);
		return $this->db->insert_id();
	}
}

/* End of file m_resiko.php */
/* Location: ./application/model/m_resiko.php */