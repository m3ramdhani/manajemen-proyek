<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_jadwal extends CI_Model {
	private $primary_key = 'id_jadwal';
	private $table_name = 'jadwal';

	function __construct(){
		parent::__construct();
	}

	function count_all(){
		return $this->db->count_all($this->table_name);
	}

	function get_all_proyek(){
		return $this->db->get($this->table_name);
	}

	function get_status(){
		return $this->db->get($this->table_name2);
	}

	function get_by_id($id_jadwal){
		$this->db->where($this->primary_key, $id_jadwal);
		return $this->db->get($this->table_name);
	}

	function insert($data){
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function update($data, $id_jadwal){
		$this->db->where($this->primary_key, $id_jadwal);
		return $this->db->update($this->table_name, $data);
	}

	function delete($id_jadwal){
		$this->db->where($this->primary_key, $id_jadwal);
		return $this->db->delete($this->table_name);
	}

	function get_jadwal($id_proyek){
		$this->db->where('id_proyek', $id_proyek);
		return $this->db->get($this->table_name);
	}

	function delete_jadwal($id_proyek){
		$this->db->where('id_proyek', $id_proyek);
		return $this->db->delete($this->table_name);
	}

	function get_successors($id_jadwal)
	{	
		$this->db->where_in('id_jadwal', $id_jadwal);
	}
}

/* End of file M_manpower.php */
/* Location: ./application/model/M_manpower.php */