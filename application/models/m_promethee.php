<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class M_promethee extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_bobot()
	{
		$this->db->select('
				id_proyek,
				py.nama_proyek,
				pf.bobot_platform,
				pc.bobot_price,
				w.bobot_waktu,
				m.bobot_manpower
			')
			->from('proyek py')
			->join('platform pf', 'pf.id_platform = py.id_platform')
			->join('price pc', 'pc.id_price = py.id_price')
			->join('waktu w', 'w.id_waktu = py.id_waktu')
			->join('manpower m', 'm.id_manpower = py.id_manpower')
			->where('id_enum_status', 1);

		return $this->db->get();
	}

	public function get_all()
	{
		$this->db->select('py.id_proyek, py.nama_proyek, py.net_flow, pf.kriteria_platform, py.mulai_proyek, py.berakhir_proyek')
				 ->from('proyek py')
				 ->join('platform pf', 'pf.id_platform = py.id_platform')
				 ->order_by('net_flow', 'DESC');
		return $this->db->get();
	}
}