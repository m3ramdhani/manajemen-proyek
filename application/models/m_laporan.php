<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan extends CI_Model {
	private $primary_key = 'id_laporan';
	private $table_name = 'laporan_proyek';

	function __construct(){
		parent::__construct();
	}

	function count_all(){
		return $this->db->count_all($this->table_name);
	}

	function get_all_resiko(){
		return $this->db->get($this->table_name);
	}

	function get_max_code(){
		$this->db->select_max('kode_resiko');
		return $this->db->get($this->table_name)->result();
	}

	function get_by_id($id_laporan){
		$this->db->where($this->primary_key, $id_laporan);
		return $this->db->get($this->table_name);
	}

	function insert($data){
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function update($data, $id_laporan){
		$this->db->where($this->primary_key, $id_laporan);
		return $this->db->update($this->table_name, $data);
	}

	function delete($id_laporan){
		$this->db->where($this->primary_key, $id_laporan);
		return $this->db->delete($this->table_name);
	}

	function insert_detail($data)
    {
        $this->db->insert('detail_laporan', $data);
		return $this->db->insert_id();
    }

    function get_detail($id_laporan)
    {
    	$this->db->select('r.kode_resiko, r.deskripsi_resiko')
    		 ->from('detail_laporan det')
    		 ->join('resiko r', 'r.id_resiko = det.id_resiko')
    		 ->where('det.id_laporan', $id_laporan);
    	return $this->db->get()->result();
    }
}

/* End of file m_resiko.php */
/* Location: ./application/model/m_resiko.php */