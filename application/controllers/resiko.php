<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resiko extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper(array('url','form', 'data'));
		$this->load->model('m_resiko');
	}

	public function index()
	{
		$data['resiko'] = $this->m_resiko->get_all_resiko();
		$this->load->view('resiko_view', $data);
	}

	public function tambah()
	{
		if ($this->input->post()) {
			$id_proyek = $this->input->post('id_proyek');
			$get_last_data = array_pop($this->m_resiko->get_max_code());
			$last_code = substr($get_last_data->kode_resiko, 1);
			$new_code = $last_code + 1;

			$probabilitas = $this->input->post('probabilitas');
			$dampak = $this->input->post('dampak');
			$tingkat_kepentingan_resiko = $probabilitas * $dampak;

			$data = array(
					'kode_resiko' => 'R'.$new_code,
					'jenis_resiko' => $this->input->post('jenis_resiko'),
					'deskripsi_resiko' => $this->input->post('deskripsi_resiko'),
					'probabilitas' => $probabilitas,
					'dampak' => $dampak,
					'tingkat_kepentingan_resiko' => $tingkat_kepentingan_resiko,
					'tindakan_pengendali' => $this->input->post('tindakan_pengendali')
				);

			$id = $this->m_resiko->insert($data);
			if ($id > 0) {
				$data_resiko = array(
					'id_proyek' => $id_proyek,
					'id_resiko' => $id
				);
				$this->m_resiko->add_resiko($data_resiko);
			}
			redirect('proyek/resiko/'.$id_proyek);
		} else {
			$this->load->view('resiko_tambah');
		}		
	}

	function update($id = 0){
		$id = $this->uri->segment(3);
		if ($this->input->post()) {
			$jenis_resiko =$this->input->post('jenis_resiko');
			$deskripsi_resiko =$this->input->post('deskripsi_resiko');
			$dampak =$this->input->post('dampak');
			$probabilitas=$this->input->post('probabilitas');
			$tindakan_pengendali=$this->input->post('tindakan_pengendali');
			$tingkat_kepentingan_resiko = $probabilitas * $dampak;

			$data = array(
				'jenis_resiko' => $jenis_resiko,
				'deskripsi_resiko'=> $deskripsi_resiko,
				'dampak'=>$dampak,
				'probabilitas'=>$probabilitas,
				'tindakan_pengendali'=>$tindakan_pengendali,
				'tingkat_kepentingan_resiko' => $tingkat_kepentingan_resiko
			);

			$this->m_resiko->update($data, $id);
			redirect('resiko/index');			
		} else {
			$data['resiko'] = $this->m_resiko->get_by_id($id)->result();
			$this->load->view('resiko_edit',$data);
		}		
	}

	function delete($id){
		//load function delete on model
		$this->m_resiko->delete($id);
		//redirect
		redirect('resiko/index');
	}
}
/* End of file resiko.php */
/* Location: ./application/controllers/resiko.php */