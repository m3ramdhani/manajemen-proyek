<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->helper(array('data', 'cpm'));
		$this->load->model('m_proyek');
		$this->load->model('m_jadwal');
	}

	public function detail($id = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['data_proyek'] = $this->m_proyek->get_by_id($id)->result();
			/*$data_jadwal = array_pop($this->m_jadwal->get_jadwal($id)->result());
			if (sizeof($data_jadwal) > 0) {
				$data['results'] = $this->hitung_maju($data_jadwal);
			}*/
			$data['tasklist'] = $this->m_jadwal->get_jadwal($id)->result();
			$data['add_url'] = base_url().'jadwal/add_jadwal/'.$id;
			$data['url_hitung'] = base_url().'criticalpath/main/'.$id;

			$this->load->view('jadwal_view', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function add_jadwal($id_proyek = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			if ($this->input->post()) {
				$insert_data = array(
					'id_proyek' => $id_proyek,
					'tasklist' => $this->input->post('tasklist'),
					'start_date' => $this->input->post('start_date'),
					'end_date' => $this->input->post('end_date'),
					'predecessor' => implode(",", $this->input->post('predecessor')),
					'duration' => harikerja($this->input->post('start_date'),$this->input->post('end_date')),
					'code' => $this->input->post('code')
				);
				$id_jadwal = $this->m_jadwal->insert($insert_data);

				if ($id_jadwal > 0) {
					redirect('jadwal/detail/'.$id_proyek, 'refresh');
				}
			}
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function add_jadwal_old()
	{
		if ($this->input->post()) {
			$params = $this->input->post();
			$working_days = harikerja($params['mulai-proyek'], $params['akhir-proyek']);

			$persiapan = harikerja($params['persiapan-awal'], $params['persiapan-akhir']);
			$analisis = harikerja($params['analisis-awal'], $params['analisis-akhir']);
			$development = harikerja($params['development-awal'], $params['development-akhir']);
			$backend = harikerja($params['backend-awal'], $params['backend-akhir']);
			$qa = harikerja($params['qa-awal'], $params['qa-akhir']);
			$support = harikerja($params['support-awal'], $params['support-akhir']);

			$data_jadwal = array_pop($this->m_jadwal->get_jadwal($params['id_proyek'])->result());
			if (sizeof($data_jadwal) > 0) {
				$id_jadwal = $this->m_jadwal->delete_jadwal($params['id_proyek']);
				if ($id_jadwal) {
					$id_jadwal = $this->m_jadwal->insert(array('persiapan' => $persiapan, 'analisis' => $analisis, 'dev_front' => $development, 'dev_back' => $backend, 'qa' => $qa, 'ts' => $support, 'id_proyek' => $params['id_proyek']));
				}
			} else {
				$id_jadwal = $this->m_jadwal->insert(array('persiapan' => $persiapan, 'analisis' => $analisis, 'dev_front' => $development, 'dev_back' => $backend, 'qa' => $qa, 'ts' => $support, 'id_proyek' => $params['id_proyek']));
			}

			if ($id_jadwal) {
				redirect('jadwal/detail/'.$params['id_proyek']);
			}
		}
	}

	public function delete($id_jadwal = 0, $id_proyek = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$this->m_jadwal->delete($id_jadwal);
			redirect('jadwal/detail/'.$id_proyek, 'refresh');
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function hitung_maju($id)
	{
		echo $id;
		$tasklist = $this->m_jadwal->get_jadwal($id)->result_array();
		echo "<br/>";
		echo "<pre>";
		print_r($tasklist);
		echo "<br/>";

		$es = array();
		$ef = array();

		for ($i=0; $i <= sizeof($tasklist); $i++) { 
			if ($i < sizeof($tasklist)) {
				if ($tasklist[$i]['predecessor'] == '') {
					$es[$i] = 0;
				} else {

				}
			}
			echo $i."<br/>";
		}
		print_r($es);

		/*$es1 = 0; 								$ef1 = $es1 + $data->persiapan;
		$es2 = 0; 								$ef2 = $es2 + $data->persiapan;
		$es3 = $ef1; 							$ef3 = $es3 + $data->analisis;
		$es4 = $ef2; 							$ef4 = $es4 + $data->dev_front;
		$es5 = ($ef3 >= $ef4) ? $ef3 : $ef4;	$ef5 = $es5 + $data->dev_back;
		$es6 = $ef5; 							$ef6 = $es6 + $data->qa;
		$es7 = $ef6;							$ef7 = $es7 + $data->ts;

		$maju = array(
			0 => $ef1,
			1 => $ef2,
			2 => $ef3,
			3 => $ef4,
			4 => $ef5,
			5 => $ef6,
			6 => $ef7
		);

		$mundur = $this->hitung_mundur($data, $ef7);

		$kode = array(
			0 => 'A',
			1 => 'A',
			2 => 'B',
			3 => 'C',
			4 => 'D',
			5 => 'E',
			6 => 'F'
		);

		$float = array();
		for ($i=0; $i < 7; $i++) { 
			$float[$i] = $mundur[$i] - $maju[$i];
		}

		$data = array(
			'float' => $float,
			'kode' => $kode,
		);

		return $data;*/
	}

	public function hitung_mundur($data, $value)
	{
		$ef7 = $value;	$es7 = $value - $data->ts;
		$ef6 = $es7;	$es6 = $ef6 - $data->qa;
		$ef5 = $es6;	$es5 = $ef5 - $data->dev_back;
		$ef4 = $es5;	$es4 = $ef4 - $data->dev_front;
		$ef3 = $es5;	$es3 = $ef3 - $data->analisis;
		$ef2 = $es4;	$es2 = $ef2 - $data->persiapan;
		$ef1 = $es3;	$es1 = $ef1 - $data->persiapan;

		$mundur = array(
			0 => $ef1,
			1 => $ef2,
			2 => $ef3,
			3 => $ef4,
			4 => $ef5,
			5 => $ef6,
			6 => $ef7
		);

		return $mundur;
	}
}
/* End of file manpower.php */
/* Location: ./application/controllers/manpower.php */