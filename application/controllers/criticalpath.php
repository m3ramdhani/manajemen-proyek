<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class CriticalPath extends CI_Controller
{
	public $na;
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_jadwal');
		$this->load->helper(array('cpm'));
	}

	public function main($id = 0)
	{
		$list = $this->getActivity($id);
		$list = $this->walkListAhead($list);
		$list = $this->walkListAback($list);

		$this->criticalPath($list);

		echo "<pre>";
		print_r($list);
	}

	public function getActivity($id = 0)
	{
		$activity = array();
		$list = array();
		$tasklist = $this->m_jadwal->get_jadwal($id)->result_array();
		$this->na = count($tasklist);

		for ($i=0; $i < $this->na; $i++) {
			$activity = array(
				'id' => $tasklist[$i]['code'],
				'description' => $tasklist[$i]['tasklist'],
				'duration' => $tasklist[$i]['duration'],
				'est' => $tasklist[$i]['duration'],
				'lst' => $tasklist[$i]['duration'],
				'eet' => $tasklist[$i]['duration'],
				'let' => $tasklist[$i]['duration']
				);

			$list_predecessor = explode(",", $tasklist[$i]['predecessor']);
			$get_predecessor = array();
            $j = 0;
			if (count($list_predecessor) > 1) {
                foreach ($list_predecessor as $value) {
                    $get_predecessor[$j] = get_predecessor($value);
                    $j++;
                }
            } else {
                $get_predecessor[$j] = get_predecessor($tasklist[$i]['predecessor']);
            }

            for ($j=0; $j < count($list_predecessor); $j++) { 
            	$aux = array();

            	if (($aux = checkActivity($list, $get_predecessor[$j], $i)) != null) {
            		$activity['predecessors'][$j] = $aux;

            		$list[getIndex($list, $aux, $i)] = setSuccessors($aux, $activity);
            	}
            }

			$list[$i] = $activity;
		}

		return $list;
	}

	public function walkListAhead($list)
	{
		$list[0]['eet'] = $list[0]['est'] + $list[0]['duration'];

		for ($i=1; $i < $this->na; $i++) { 
			foreach ($list[$i]['predecessors'] as $activity) {
				if ($list[$i]['est'] < $activity['eet'])
					$list[$i]['est'] = $activity['eet'];
			}

			$list[$i]['eet'] = $list[$i]['est'] + $list[$i]['duration'];
		}

		return $list;
	}

	public function walkListAback($list)
	{
		$list[$this->na - 1]['let'] = $list[$this->na - 1]['eet'];
		$list[$this->na - 1]['lst'] = $list[$this->na - 1]['let'] - $list[$this->na - 1]['duration'];

		for ($i=$this->na - 2; $i >= 0; $i--) { 
			foreach ($list[$i]['successors'] as $activity) {
				if ($list[$i]['let'] == 0)
					$list[$i]['let'] = $activity['lst'];
				else
					if ($list[$i]['let'] > $activity['lst'])
						$list[$i]['let'] = $activity['lst'];
			}

			$list[$i]['lst'] = $list[$i]['let'] - $list[$i]['duration'];
		}

		return $list;
	}

	public function criticalPath($list)
	{
		foreach ($list as $activity) {
			if (($activity['eet'] - $activity['let'] == 0) && ($activity['est'] - $activity['lst'] == 0))
				echo $activity['id']." ";
		}
	}
}