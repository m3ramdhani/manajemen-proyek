<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Promethee extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_proyek');
		$this->load->model('m_promethee');
	}

	public function run()
	{
		if ($this->input->is_ajax_request()) {
			$get_proyek = $this->m_promethee->get_bobot()->result();
			
			/**
			*	$i = 0 	=> platform
			*	$i = 1	=> price
			*	$i = 2	=> waktu
			*	$i = 3	=> manpower
			*/
			$array_platform = array();
			$array_price = array();
			$array_waktu = array();
			$array_manpower = array();
			for ($i=0; $i < 4; $i++) {
				foreach ($get_proyek as $proyek) {
					$y = 0;
					foreach ($get_proyek as $p) {
						switch ($i) {
							case 0:
								$diff = $proyek->bobot_platform - $p->bobot_platform;
								$array_platform[$proyek->id_proyek][$y] = ($diff > 0) ? 1 : 0;
								break;
							case 1:
								$diff = $proyek->bobot_price - $p->bobot_price;
								$array_price[$proyek->id_proyek][$y] = ($diff > 0) ? 1 : 0;
								break;
							case 2:
								$diff = $proyek->bobot_waktu - $p->bobot_waktu;
								$array_waktu[$proyek->id_proyek][$y] = ($diff > 0) ? 1 : 0;
								break;
							default:
								$diff = $proyek->bobot_manpower - $p->bobot_manpower;
								$array_manpower[$proyek->id_proyek][$y] = ($diff > 0) ? 1 : 0;
								break;
						}
						$y++;
					}
				}
			}

			$sum1 = $this->sum_array($get_proyek, sizeof($get_proyek), $array_platform, $array_price);
			$sum2 = $this->sum_array($get_proyek, sizeof($get_proyek), $sum1, $array_waktu);
			$sum3 = $this->sum_array($get_proyek, sizeof($get_proyek), $sum2, $array_manpower);

			$index_preferensi = $this->preferensi($sum3);

			foreach ($get_proyek as $proyek) {
				$x = 0;
				foreach ($index_preferensi as $value) {
					$x++;
				}
			}

			$leaving_flow = $this->leaving_flow($get_proyek, $index_preferensi);
			$entering_flow = $this->entering_flow($get_proyek, $index_preferensi);
			$net_flow = $this->net_flow($get_proyek, $leaving_flow, $entering_flow);
			arsort($net_flow);
			
			foreach ($net_flow as $key => $value) {
				$this->m_proyek->update(array('net_flow' => floatval($value), 'id_enum_status' => 2), $key);
			}

			$proyek = $this->m_promethee->get_all()->result();
			
			$html = "";
			foreach ($proyek as $key) {
				$html .= "<tr class='odd gradeX'>";
				$html .= "<td>".$key->nama_proyek."</td>";
				$html .= "<td>".$key->kriteria_platform."</td>";
				$html .= "<td>".$key->mulai_proyek."</td>";
				$html .= "<td>".$key->berakhir_proyek."</td>";
				$html .= "</tr>";
			}

			echo json_encode(array(
				"data" => $html,
				"status" => true
			));
		}	
	}

	public function sum_array($proyek, $size, $array1, $array2)
	{
		$sumArray = array();
		foreach ($proyek as $p) {
			for ($i=0; $i < $size; $i++) { 
				$sumArray[$p->id_proyek][$i] = $array1[$p->id_proyek][$i] + $array2[$p->id_proyek][$i];
			}
		}

		return $sumArray;
	}

	public function preferensi($array)
	{
		
		foreach ($array as $key => $value) {
			$i = 0;
			foreach ($value as $v) {
				$array[$key][$i] = (1/4) * $v;
				$i++;
			}
		}
		
		return $array;
	}

	public function leaving_flow($proyek, $array)
	{
		$leaving_flow = array();
		foreach ($proyek as $p) {
			$lv = (1/(sizeof($proyek)-1)) * array_sum($array[$p->id_proyek]);
			$leaving_flow[$p->id_proyek] = $lv;
		}

		return $leaving_flow;
	}

	public function entering_flow($proyek, $array)
	{
		$entering_flow = array();
		$x = 0;
		foreach ($proyek as $p) {
			$sum[$p->id_proyek] = 0;
			foreach ($array as $a) {
				$sum[$p->id_proyek] += $a[$x];
				$entering_flow[$p->id_proyek] = (1/(sizeof($proyek)-1)) * $sum[$p->id_proyek];
			}
			$x++;
		}

		return $entering_flow;
	}

	public function net_flow($proyek, $leaving_flow, $entering_flow)
	{
		$sumArray = array();
		foreach ($proyek as $p) {
			$sumArray[$p->id_proyek] = $leaving_flow[$p->id_proyek] - $entering_flow[$p->id_proyek];
		}

		return $sumArray;
	}
}