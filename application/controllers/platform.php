<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Platform extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->load->model('m_platform');	
	}

	public function index()
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['platform'] = $this->m_platform->get_all_platform();
			$this->load->view('platform_view', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function tambah()
	{
		if ($this->input->post()) {
			$data = array('kriteria_platform' =>$this->input->post('kriteria_platform'),
						  'bobot_platform' =>$this->input->post('bobot_platform')
						  );
			$id = $this->m_platform->insert($data);
			redirect('platform/index');
		} else {
			$this->load->view('platform_tambah');
		}		
	}

	function update($id = 0){
		$id = $this->uri->segment(3);
		if ($this->input->post()) {
			$kriteria_platform =$this->input->post('kriteria_platform');
			$bobot_platform =$this->input->post('bobot_platform');

			$data = array(
				'kriteria_platform' => $kriteria_platform,
				'bobot_platform'=> $bobot_platform
			);

			$this->m_platform->update($data, $id);
			redirect('platform/index');			
		} else {
			$data['platform'] = $this->m_platform->get_by_id($id)->result();
			$this->load->view('platform_edit',$data);
		}		
	}

	function delete($id){
		//load function delete on model
		$this->m_platform->delete($id);
		//redirect
		redirect('platform/index');
	}
}

/* End of file platform.php */
/* Location: ./application/controllers/platform.php */