<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->load->model('m_login', '', TRUE);		
	}

	public function index()
	{
		if($this->session->userdata('login')==TRUE){
			redirect('dashboard');
		}else{
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function process_login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE) {
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));

			if ($this->m_login->check_user($username, $password) == TRUE) {
				$userdata = array_pop($this->db->get_where('user', array('username' => $username, 'password' => $password))->result());
				$data = array(
					'username' => $username,
					'login' => TRUE,
					'id_access' => $userdata->id_access
				);
				$this->session->set_userdata($data);
				redirect('dashboard');
			} else {
				$data['message'] = $this->session->set_flashdata('message', 'Maaf, username dan/atau password tidak cocok.');
				redirect('login');
			}
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function process_logout()
	{
		$data = array(
			'username' => '',
			'login' => FALSE
		);
		$this->session->unset_userdata($data);
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}
}
	

/* End of file login.php */
/* Location: ./application/controllers/login.php */