<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manpower extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->load->model('m_manpower');	
	}

	public function index()
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['manpower'] = $this->m_manpower->get_all_manpower();
			$this->load->view('manpower_view',$data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function tambah()
	{
		if ($this->input->post()) {
			$data = array('kriteria_manpower' =>$this->input->post('kriteria_manpower'),
						  'bobot_manpower' =>$this->input->post('bobot_manpower')
						  );
			$id = $this->m_manpower->insert($data);
			redirect('manpower/index');
		} else {
			$this->load->view('manpower_tambah');
		}		
	}

	function update($id = 0){
		$id = $this->uri->segment(3);
		if ($this->input->post()) {
			$kriteria_manpower =$this->input->post('kriteria_manpower');
			$bobot_manpower =$this->input->post('bobot_manpower');

			$data = array(
				'kriteria_manpower' => $kriteria_manpower,
				'bobot_manpower'=> $bobot_manpower
			);

			$this->m_manpower->update($data, $id);
			redirect('manpower/index');			
		} else {
			$data['manpower'] = $this->m_manpower->get_by_id($id)->result();
			$this->load->view('manpower_edit',$data);
		}		
	}

	function delete($id){
		//load function delete on model
		$this->m_manpower->delete($id);
		//redirect
		redirect('manpower/index');
	}
}
/* End of file manpower.php */
/* Location: ./application/controllers/manpower.php */