<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Price extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->load->model('m_price');	
	}

	public function index()
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['price'] = $this->m_price->get_all_price();
			$this->load->view('price_view', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function tambah()
	{
		if ($this->input->post()) {
			$data = array('kriteria_price' =>$this->input->post('kriteria_price'),
						  'bobot_price' =>$this->input->post('bobot_price'),
						  'min_price' =>$this->input->post('min_price'),
						  'max_price' =>$this->input->post('max_price')
						  );
			$id = $this->m_price->insert($data);
			redirect('price/index');
		} else {
			$this->load->view('price_tambah');
		}		
	}

	function update($id = 0){
		$id = $this->uri->segment(3);
		if ($this->input->post()) {
			$kriteria_price =$this->input->post('kriteria_price');
			$bobot_price =$this->input->post('bobot_price');
			$min_price =$this->input->post('min_price');
			$max_price =$this->input->post('max_price');

			$data = array(
				'kriteria_price' => $kriteria_price,
				'bobot_price'=> $bobot_price,
				'min_price'=> $min_price,
				'max_price'=> $max_price
			);

			$this->m_price->update($data, $id);
			redirect('price/index');			
		} else {
			$data['price'] = $this->m_price->get_by_id($id)->result();
			$this->load->view('price_edit',$data);
		}		
	}

	function delete($id){
		//load function delete on model
		$this->m_price->delete($id);
		//redirect
		redirect('price/index');
	}
}

/* End of file price.php */
/* Location: ./application/controllers/price.php */