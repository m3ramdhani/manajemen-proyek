<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Waktu extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->load->model('m_waktu');	
	}

	public function index()
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['waktu'] = $this->m_waktu->get_all_waktu();
			$this->load->view('waktu_view',$data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function tambah()
	{
		if ($this->input->post()) {
			$data = array('kriteria_waktu' =>$this->input->post('kriteria_waktu'),
						  'bobot_waktu' =>$this->input->post('bobot_waktu'),
						  'min_waktu' =>$this->input->post('min_waktu'),
						  'max_waktu' =>$this->input->post('max_waktu')
						  );
			$id = $this->m_waktu->insert($data);
			redirect('waktu/index');
		} else {
			$this->load->view('waktu_tambah');
		}		
	}

	function update($id = 0){
		$id = $this->uri->segment(3);
		if ($this->input->post()) {
			$kriteria_waktu =$this->input->post('kriteria_waktu');
			$bobot_waktu =$this->input->post('bobot_waktu');
			$min_waktu =$this->input->post('min_waktu');
			$max_waktu =$this->input->post('max_waktu');

			$data = array(
				'kriteria_waktu' => $kriteria_waktu,
				'bobot_waktu'=> $bobot_waktu,
				'min_waktu'=> $min_waktu,
				'max_waktu'=> $max_waktu
			);

			$this->m_waktu->update($data, $id);
			redirect('waktu/index');			
		} else {
			$data['waktu'] = $this->m_waktu->get_by_id($id)->result();
			$this->load->view('waktu_edit',$data);
		}		
	}

	function delete($id){
		//load function delete on model
		$this->m_waktu->delete($id);
		//redirect
		redirect('waktu/index');
	}
}

/* End of file waktu.php */
/* Location: ./application/controllers/waktu.php */