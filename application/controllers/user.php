<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->load->model('m_user');	
	}
	
	public function index()
	{
		//load semua data user
		$data['data'] = $this->m_user->get_all_user();

		//load view
		$this->load->view('user_view', $data);
	}


	public function tambah()
	{
		if ($this->input->post()) {
			$data = array('nama_user' =>$this->input->post('nama_user'),
						  'alamat_user' =>$this->input->post('alamat_user'),
						  'telp_user' =>$this->input->post('telp_user'),
						  'email_user' =>$this->input->post('email_user'),
						  'username' =>$this->input->post('username'),
						  'password' => md5($this->input->post('password'))
						  );
			$id = $this->m_user->insert($data);
			redirect('user/index');
		} else {
			$this->load->view('user_tambah');
		}		
	}

	function update($id = 0){
		$id = $this->uri->segment(3);
		if ($this->input->post()) {
			$nama_user =$this->input->post('nama_user');
			$alamat_user =$this->input->post('alamat_user');
			$telp_user =$this->input->post('telp_user');
			$email_user =$this->input->post('email_user');
			$username =$this->input->post('username');
			$password = md5($this->input->post('password'));

			$data = array(
				'nama_user' => $nama_user,
				'alamat_user'=> $alamat_user,
				'telp_user'=> $telp_user,
				'email_user'=> $email_user,
				'username'=> $username,
				'password'=> $password
			);

			$this->m_user->update($data, $id);
			redirect('user/index');			
		} else {
			$data['user'] = $this->m_user->get_by_id($id)->result();
			$this->load->view('user_edit',$data);
		}		
	}
	function delete($id){
		//load function delete on model
		$this->m_user->delete($id);
		//redirect
		redirect('user/index');
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */