<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proyek extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_platform');
		$this->load->model('m_manpower');
		$this->load->model('m_proyek');
		$this->load->model('m_resiko');
		$this->load->model('m_laporan');
		$this->load->helper(array('form', 'data'));
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['data_proyek'] = $this->m_proyek->get_proyek();
			$data['proyek_prioritas'] = $this->m_proyek->get_proyek(0, 1);
			$this->load->view('proyek_view', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function tambah()
	{
		if ($this->input->post()) {
			$price = $this->input->post('harga_proyek');
			$start_date = date_create($this->input->post('mulai_proyek'));
			$end_date = date_create($this->input->post('berakhir_proyek'));
			$interval = date_diff($start_date, $end_date);
			$days = $interval->format('%a');

			$id_price = 0;
			if ($price <= 50000000) {
				$id_price = 1;
			} elseif ($price <= 100000000) {
				$id_price = 2;
			} elseif ($price <= 250000000) {
				$id_price = 3;
			} elseif ($price <= 400000000) {
				$id_price = 4;
			} elseif ($price <= 700000000) {
				$id_price = 5;
			} else {
				$id_price = 6;
			}

			$id_waktu = 0;
			if ($days <= 60) {
				$id_waktu = 1;
			} elseif ($days <= 120) {
				$id_waktu = 2;
			} elseif ($days <= 180) {
				$id_waktu = 3;
			} else {
				$id_waktu = 4;
			}

			$data = array(
				'nama_proyek' => $this->input->post('nama_proyek'),
				'pic_proyek' => $this->input->post('pic_proyek'),
				'alamat_proyek' => $this->input->post('alamat_proyek'),
				'telp_proyek' => $this->input->post('telp_proyek'),
				'harga_proyek' => $price,
				'mulai_proyek' => $this->input->post('mulai_proyek'),
				'berakhir_proyek' => $this->input->post('berakhir_proyek'),
				'id_price' => $id_price,
				'id_platform' => $this->input->post('platform_list'),
				'id_waktu' => $id_waktu,
				'id_manpower' => $this->input->post('manpower_list')
			);
			$id = $this->m_proyek->insert($data);
			redirect('proyek', 'refresh');
		} else {
			$data['platform'] = $this->m_platform->get_all_platform()->result();
			$data['manpower'] = $this->m_manpower->get_all_manpower()->result();
			$this->load->view('proyek_tambah', $data);
		}

	}

	function update($id = 0){
		$id = $this ->uri->segment(3);
		if ($this->input->post()) {
			$price = $this->input->post('harga_proyek');
			$start_date = date_create($this->input->post('mulai_proyek'));
			$end_date = date_create($this->input->post('berakhir_proyek'));
			$interval = date_diff($start_date, $end_date);
			$days = $interval->format('%a');

			$id_price = 0;
			if ($price <= 50000000) {
				$id_price = 1;
			} elseif ($price <= 100000000) {
				$id_price = 2;
			} elseif ($price <= 250000000) {
				$id_price = 3;
			} elseif ($price <= 400000000) {
				$id_price = 4;
			} elseif ($price <= 700000000) {
				$id_price = 5;
			} else {
				$id_price = 6;
			}

			$id_waktu = 0;
			if ($days <= 60) {
				$id_waktu = 1;
			} elseif ($days <= 120) {
				$id_waktu = 2;
			} elseif ($days <= 180) {
				$id_waktu = 3;
			} else {
				$id_waktu = 4;
			}

			$data = array(
				'nama_proyek' => $this->input->post('nama_proyek'),
				'pic_proyek' => $this->input->post('pic_proyek'),
				'alamat_proyek' => $this->input->post('alamat_proyek'),
				'telp_proyek' => $this->input->post('telp_proyek'),
				'harga_proyek' => $price,
				'mulai_proyek' => $this->input->post('mulai_proyek'),
				'berakhir_proyek' => $this->input->post('berakhir_proyek'),
				'id_price' => $id_price,
				'id_platform' => $this->input->post('platform_list'),
				'id_waktu' => $id_waktu,
				'id_manpower' => $this->input->post('manpower_list')
			);

			$update = $this->m_proyek->update($data, $this->input->post('id_proyek'));
			redirect('proyek', 'refresh');
		} else {
			$data['platform'] = $this->m_platform->get_all_platform()->result();
			$data['manpower'] = $this->m_manpower->get_all_manpower()->result();
			$data['proyek'] = $this->m_proyek->get_by_id($id)->result();
			$this->load->view('proyek_edit', $data);
		}		
	}

	public function detail($id = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['platform'] = $this->m_platform->get_all_platform()->result();
			$data['manpower'] = $this->m_manpower->get_all_manpower()->result();
			$data['proyek'] = $this->m_proyek->get_by_id($id)->result();

			$this->load->view('proyek_detail', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function delete($id = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$id = $this->uri->segment(3);

			$delete = $this->m_proyek->delete($id);

			if ($delete) {
				redirect('proyek');
			}
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function resiko($id = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['data_proyek'] = $this->m_proyek->get_by_id($id)->result();
			$data['data_resiko'] = $this->m_resiko->get_all_resiko()->result();
			$data['resiko_proyek'] = $this->m_resiko->get_resiko_proyek($id);
			$this->load->view('resiko_proyek', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function add_resiko($id_proyek = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$resiko = $this->input->post('resiko');
			$get_resiko = $this->m_resiko->get_resiko_proyek($id_proyek);
			if (sizeof($get_resiko) > 0) {
				$delete = $this->m_resiko->delete_resiko($id_proyek);
				if ($delete) {
					foreach ($resiko as $value) {
						$data = array(
							'id_proyek' => $id_proyek,
							'id_resiko' => $value
						);
						$id = $this->m_resiko->add_resiko($data);
					}
				}
			} else {
				foreach ($resiko as $value) {
					$data = array(
						'id_proyek' => $id_proyek,
						'id_resiko' => $value
					);
					$id = $this->m_resiko->add_resiko($data);
				}
			}
			redirect('proyek/resiko/'.$id_proyek, 'refresh');
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function laporan($id_proyek = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$update = $this->m_proyek->update(array('id_enum_status' => 3), $id_proyek);
			if ($update) {
				$data['data_proyek'] = $this->m_proyek->get_by_id($id_proyek)->result();
				$data['laporan'] = $this->m_proyek->get_laporan($id_proyek);
				$data['url'] = base_url().'proyek/add_laporan/'.$id_proyek;
				$data['back_url'] = base_url().'proyek';
				$this->load->view('laporan', $data);
			}
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
		
	}

	public function add_laporan($id_proyek = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			if ($this->input->post()) {
				$resiko = $this->input->post('resiko');
				$is_resiko = $this->input->post('is_resiko');

				$data = array(
					'id_proyek' => $id_proyek,
					'deskripsi_laporan' => $this->input->post('deskripsi_laporan'),
					'tanggal_laporan' => $this->input->post('tanggal_laporan')
				);
				$id_laporan = $this->m_laporan->insert($data);

				if ($id_laporan > 0 && $is_resiko === '1') {
					foreach ($resiko as $value) {
						$data = array(
							'id_laporan' => $id_laporan,
							'id_resiko' => $value
						);
						$laporan = $this->m_laporan->insert_detail($data);
					}
				}

				redirect('proyek/laporan/'.$id_proyek);
			}

			$data['proyek'] = array_pop($this->m_proyek->get_by_id($id_proyek)->result());
			$data['list_resiko'] = $this->m_resiko->get_resiko_proyek($id_proyek);
			$data['url'] = base_url().'proyek/add_laporan/'.$id_proyek;
			$data['back_url'] = base_url().'proyek/laporan/'.$id_proyek;
			$data['resiko_yes'] = array('name' => 'is_resiko',
				'id' => 'resiko_yes',
	            'type' => 'radio',
	            'class' => 'requiredTextField',
	            'field-name' => 'Resiko',
	            'placeholder' => '',
	            'value' => 1);
	        $data['resiko_no'] = array('name' => 'is_resiko',
				'id' => 'resiko_no',
	            'type' => 'radio',
	            'class' => 'requiredTextField',
	            'field-name' => 'Resiko',
	            'placeholder' => '',
	            'value' => 0);

			$this->load->view('laporan_tambah', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function detail_laporan($id_laporan = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$data['detail'] = $this->m_laporan->get_detail($id_laporan);
			$data['laporan'] = array_pop($this->m_laporan->get_by_id($id_laporan)->result());
			$data['back_url'] = base_url().'proyek/laporan/'.$data['laporan']->id_proyek;
			$this->load->view('laporan_detail', $data);
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}

	public function done($id_proyek = 0)
	{
		if ($this->session->userdata('login') == TRUE) {
			$update = $this->m_proyek->update(array('id_enum_status' => 4), $id_proyek);
			if ($update) {
				redirect('proyek');
			}
		} else {
			$data['message'] = $this->session->flashdata('message');
			$data['action'] = 'login/process_login';
			$this->load->view('login_view', $data);
		}
	}
}

/* End of file platform.php */
/* Location: ./application/controllers/platform.php */